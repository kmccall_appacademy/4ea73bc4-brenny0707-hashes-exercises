# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words_hash = Hash.new()
  str.split(" ").each do |word|
    words_hash[word] = word.length
  end
  words_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |key, value| value}[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { | key, value | older[key] = value }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  word_count = Hash.new(0)
  word.split("").each { |letter| word_count[letter] += 1 }
  word_count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
arr_count = Hash.new(0)
  arr.each { |ele| arr_count[ele] += 1 }
  arr_count.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_odd = Hash.new(0)

  numbers.each do |number|
    even_odd[:even] += 1 if number % 2 == 0
    even_odd[:odd] += 1 if number % 2 == 1
  end
  even_odd
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_histogram = Hash.new(0)
  string.split("").each do |letter|
    vowel_histogram[letter] += 1 if "aeiou".split("").include?(letter)
  end
  vowel_histogram.sort_by { |key, value| value}[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  bday_students = Hash.new()

  students.each do |student, bday|
    bday_students[student] = bday if bday >= 7
  end
  bday_students.keys.combination(2)
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  animal_count = Hash.new(0)

  specimens.each { |animal| animal_count[animal] += 1}
  sorted_count = animal_count.sort_by { |animal, number| number }
  sorted_count.length ** 2 * sorted_count[0][-1] / sorted_count[-1][-1]
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_letters = normal_sign.downcase.delete("!?.,:;' ")
  vandalized_letters = vandalized_sign.downcase.delete("!?.,:; ")
  normal_histogram = Hash.new(0)
  vandalized_histogram = Hash.new(0)

  normal_letters.split("").each { |letter| normal_histogram[letter] += 1}
  vandalized_letters.split("").each { |letter| vandalized_histogram[letter] += 1}
  vandalized_histogram.each do |letter, count|
    return false if vandalized_histogram[letter] > normal_histogram[letter]
  end
  true
end

def character_count(str)
end
